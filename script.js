var printWidthInput = document.getElementById('print-width');
var printHeightInput = document.getElementById('print-height');
var imageWidthInput = document.getElementById('image-width');
var imageHeightInput = document.getElementById('image-height');

var calculateButton = document.getElementById('calculate-button');

var imageResultWidth = document.getElementById('image-result-width');
var imageResultHeight = document.getElementById('image-result-height');

var dieResultWidth = document.getElementById('die-result-width');
var dieResultHeight = document.getElementById('die-result-height');

calculateButton.addEventListener('click', function() {
    calculate(printWidthInput.value, printHeightInput.value, imageWidthInput.value, imageHeightInput.value);
}, false)

function calculate(pWidth, pHeight, iWidth, iHeight) {

    var margin = 0.125;

    var printWidth = pWidth;
    var printHeight = pHeight;

    var printArea = printWidth * printHeight;

    var imageWidth = iWidth;
    var imageHeight = iHeight;

    var imageArea = imageWidth * imageHeight;

    var squareRoot = Math.sqrt(printArea / imageArea);

    var newWidth = (imageWidth * squareRoot).toFixed(3);
    var newHeight = (imageHeight * squareRoot).toFixed(3);

    console.log(newWidth)

    imageResultWidth.value = (newWidth - (margin * 2)) + ' in';
    imageResultHeight.value = (newHeight - (margin * 2)) + ' in';
    
    dieResultWidth.value = newWidth + ' in';
    dieResultHeight.value = newHeight + ' in';
}